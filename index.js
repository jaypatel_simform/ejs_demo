const express = require('express');
const app = express();
const i18next = require('i18next');
const i18nextMiddleware = require('i18next-http-middleware');
const Backend = require('i18next-node-fs-backend');
const port = process.nextTick.PORT || 8080


i18next
    .use(i18nextMiddleware.LanguageDetector)
    .use(Backend)
    .init({
        fallBackLng: 'en',
        preload: ['en', 'ru'],
        backend: {
            loadPath: './locales/{{lng}}/translation.json'
        },
    });
i18next.locale = "en"

app.use(i18nextMiddleware.handle(i18next))
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    console.log("list", req.t('data', { returnObjects: true }))
    res.render('pages/index', {
        pizzaList: req.t('data', { returnObjects: true }),
        tagline: req.t('tagline')
    });
});

app.get('/about', function (req, res) {
    res.render('pages/about', {
        header: req.t('header', { returnObjects: true }),
        img: "https://www.businessupturn.com/wp-content/uploads/2021/07/Untitled-design-19-7.jpg",
        heighlight: req.t('heighlight', { returnObjects: true })
    });
});

app.listen(port, () => {
    console.log(`server is running on port : ${port}`);
});